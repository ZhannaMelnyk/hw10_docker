**From Docker Hub:**

`docker pull zhannamelnyk/hw10_docker`

`docker run -it -p 3000:3000 zhannamelnyk/hw10_docker`

**From Bitbucket:**

`git clone https://bitbucket.org/ZhannaMelnyk/hw10_docker.git`

`cd hw10_docker`

`docker build -t hw10_docker .`

`docker-compose up`