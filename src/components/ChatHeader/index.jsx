import React from 'react';
import PropTypes from 'prop-types';
import './index.css';

class ChatHeader extends React.Component {
  render() {
    return (
      <div className='chat__header'>
        <h2 className='chat__header-title'>My chat</h2>
        <span className='chat__header-partisipants'>{this.props.usersAmount} partiсipants</span>
        <span className='chat__header-messages'>{this.props.messageAmount} messages</span>
        <span className='chat__header-last-message'>last message at {this.props.lastMessageTime}</span>
      </div>
    )
  }
}

ChatHeader.propTypes = {
  usersAmount: PropTypes.number,
  messageAmount: PropTypes.number,
  lastMessageTime: PropTypes.string
}

export default ChatHeader;